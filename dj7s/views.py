from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect

def index(request):
    return render(request, 'dj7s/index.html', {"nbar":"index"})

def who_we_are(request):
    return render(request, 'dj7s/Who_we_are.html', {"nbar":"who"})

def matrix(request):
    return render(request, 'dj7s/Matrix.html', {"nbar":"how"})

def innovation_culture(request):
    return render(request, 'dj7s/Innovation-culture.html', {"nbar":"who"})
