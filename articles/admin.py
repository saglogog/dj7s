from django.contrib import admin
from .models import Taxonomy, Keyword, Image, Article

# Register your models here.

class ImageInline(admin.TabularInline):
    model = Image
    extra = 1

# class KeywordInline(admin.TabularInline):
#     model = Keyword.article.through
#     extra = 1
#     search_fields = ['keyword_name']


class TaxonomyInline(admin.TabularInline):
    ''' To display keyword information on article.
        see: https://docs.djangoproject.com/en/2.0/ref/contrib/admin/#working-with-many-to-many-intermediary-models
    '''

    model = Taxonomy
    autocomplete_fields = ['keyword']
    model._meta.verbose_name_plural = "Taxonomies"
    # model = Keyword.article.through
    # autocomplete_fields = ['keyword']
    # list_display = ('keyword_desc', )
    extra = 1


class ArticleAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                {'fields': ['article_title', 'article_body'], 'classes': ( 'wide'),},),
        ('Date Information',  {'fields': ['pub_date']}),
    ]
    inlines = [ImageInline, TaxonomyInline]
    list_display = ('article_title', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    # prepopulated_fields = {'article_slug': ['article_title', ]}


class KeywordAdmin(admin.ModelAdmin):
    """ We need KeywordAdmin in order to show the autocomplete fields
        in TaxonomyInline (the following search_fields dict is required.
        However, we do not want to show the Keyword Admin model.
        Thus we add the get_model_perms method as described in
        (https://stackoverflow.com/questions/2431727/django-admin-hide-a-model)
    """
    search_fields = ['keyword_name']
    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}




admin.site.register(Article, ArticleAdmin)
admin.site.register(Keyword, KeywordAdmin )
