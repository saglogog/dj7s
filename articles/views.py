from django.shortcuts import render
from django.http import HttpResponse
from django.views import generic


from .models import Article, Image, Keyword, Taxonomy
# Create your views here.

# def index(request):
#     latest_article_list = Article.objects.order_by('-pub_date')[:5]
#     context = {'latest_article_list': latest_article_list, "nbar":"news"}
#     return render(request, 'articles/index.html', context)

class ArticleIndex(generic.ListView):
    template_name = 'articles/index.html'
    context_object_name = 'latest_article_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Article.objects.order_by('-pub_date')[:5]

    def get_context_data(self, **kwargs):
        """ This extends the generic view, adding the related images to the
        returned context:
        https://docs.djangoproject.com/en/2.0/topics/class-based-views/generic-display/#adding-extra-context
        """
        context = super().get_context_data(**kwargs)
        context['nbar'] = 'news'
        return context



# def detail(request):
#     return render(request, 'articles/detail.html', context)

class ArticleDetail(generic.DetailView):
    model = Article
    template_name = 'articles/detail.html'

    def get_context_data(self, **kwargs):
        """ This extends method override(?) the generic view, adding the related images to the
        returned context.
        """
        context = super().get_context_data(**kwargs)
        context['images_list'] = Image.objects.filter(article__id=self.get_object().pk)
        context['keywords_list'] = Keyword.objects.filter(taxonomy__article=self.get_object().pk)
        return context



class KeywordIndex(generic.ListView):
    template_name = 'articles/keyword.html'
    context_object_name = 'keyword_article_list'

    def get_queryset(self):
        """Return articles with with that contain the same keyword."""
        # keyword_pr_key = Keyword.objects.get(keyword_name=self.kwargs['the_keyword_name'])
        # pr_key = Keyword.objects.filter(keyword_name='forest')
        # object_list = Taxonomy.objects.filter(keyword_name = self.kwargs['the_keyword_name'])
        return Article.objects.filter(keyword__keyword_name=self.kwargs['the_keyword_name'])
