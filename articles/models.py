import datetime
import re

from django.db import models
from django.utils import timezone

from ckeditor.fields import RichTextField


# Create your models here.


def slug_default(article_title):
    # if character not in [0-9a-zA-Z], or one of $-_.+!*'(), it is converted to underscore
    slug_name=""
    for char in article_title[:50]:
        if not re.match("[a-zA-z0-9$-_+()]", char):
            char='_'
        slug_name += char
    return slug_name



class Article(models.Model):
    article_title = models.CharField(max_length=200, default="")
    pub_date = models.DateTimeField('date published', default=timezone.now)
    article_slug = models.SlugField(max_length=50, default=slug_default(str(article_title)))
    article_body = RichTextField(default="")

    def __str__(self):
        return self.article_title

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'



class Image(models.Model):
    image_name = models.CharField(max_length=200)
    article_image = models.ImageField(upload_to='dj7s/article_images/%Y/%m/%d', blank=True)
    image_alt = models.CharField(max_length=200, blank=True)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    def __str__(self):
        return self.image_name



class Keyword(models.Model):
    keyword_name = models.CharField(max_length=200, unique=True)
    keyword_desc = models.CharField(max_length=500)
    # article = models.ForeignKey(Article, on_delete=models.CASCADE)
    taxonomies = models.ManyToManyField(Article, through="Taxonomy")
    def __str__(self):
        return self.keyword_name



class Taxonomy(models.Model):
    """ Represents intermediate table of many to many Article-Keyword relationship."""
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    keyword = models.ForeignKey(Keyword, on_delete=models.CASCADE)

# add taxonomy and maybe make fields names unique
