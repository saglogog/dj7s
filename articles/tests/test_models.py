from django.test import TestCase
from articles.models import Article, Image, Keyword, Taxonomy



class ArticleTestCase(TestCase):
    # fixtures = ['fixture_1.json']

    # Arrange
    @classmethod
    def setUpTestData(cls):
        # Runs for once for all class methods to set up test data.

        Article.objects.create(article_title='test article', article_body='test body',
                                pub_date='2018-02-18', article_slug='test_article_slug')

    def setUp(self):
        # Runs once for each class method to setup clean data.
        pass

    def test_article_name(self):
        # Act
        article = Article.objects.get(id=1)
        field_label = article._meta.get_field('article_title').verbose_name
        # Assert
        self.assertEqual(field_label, 'article title')

    def test_article_slug(self):
        pass

    def test_article_body(self):
        pass

    def test_pub_date(self):
        pass

    def test_was_published_recently(self):
        pass



class ImageTestCase(TestCase):
    def setUp(self):
        pass

    def test_random_test(self):
        pass



class KeywordTestCase(TestCase):
    def setUp(self):
        pass

    def test_random_test(self):
        pass



class KeywordTestCase(TestCase):
    def setUp(self):
        pass

    def test_random_test(self):
        pass
