from django.urls import path

from . import views

app_name = 'articles'
urlpatterns = [
    path('', views.ArticleIndex.as_view(), name='index'),
    # old index view, as method
    # path('', views.index, name='index'),
    path('<int:pk>/', views.ArticleDetail.as_view(), name='detail'),
    # keywords path, keyword name is described as unique in the model
    path('keywords/<str:the_keyword_name>/', views.KeywordIndex.as_view(), name='keyword'),
]


# try to make article detail urls from article slugs
# https://docs.djangoproject.com/en/2.0/ref/contrib/admin/#django.contrib.admin.ModelAdmin.prepopulated_fields
# https://docs.djangoproject.com/en/2.0/topics/http/urls/
# https://docs.djangoproject.com/en/2.0/ref/models/fields/#slugfield
